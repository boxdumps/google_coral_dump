## coral-user 12 SQ1A.220205.002 8010174 release-keys
- Manufacturer: google
- Platform: msmnile
- Codename: coral
- Brand: google
- Flavor: coral-user
- Release Version: 12
- Id: SQ1A.220205.002
- Incremental: 8010174
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/coral/coral:12/SQ1A.220205.002/8010174:user/release-keys
- OTA version: 
- Branch: coral-user-12-SQ1A.220205.002-8010174-release-keys-random-text-28175192309889
- Repo: google_coral_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
